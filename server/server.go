package main

import "fmt"
import "net"
import "encoding/binary"
import "time"

type User struct {
	Name string
	Conn *net.Conn
}

type Room struct {
	Name string
	Users map[int]*User
	LastUser int
}

var Rooms map[int]*Room
var UserLobby map[int]*User
var LastUser int
var LastRoom int

func main() {
	Rooms = make(map[int]*Room)
	UserLobby = make(map[int]*User)

	LastUser = 1
	LastRoom = 5

	Rooms[1] = &Room{Name:"Default", Users:make(map[int]*User)}
	Rooms[2] = &Room{Name:"Not Deafult", Users:make(map[int]*User)}
	Rooms[4] = &Room{Name:"Absolutely Not Deafult", Users:make(map[int]*User)}

	l, err := net.Listen("tcp", "localhost:13646")
	if err != nil {
		fmt.Printf("Network Failed")
		panic(err)
	}
	defer l.Close()

	go UserLobbyIterator()

	for {
		conn, err := l.Accept()
		if err != nil {
			continue
		}

		go HandleConnection(conn)
	}

}

func GetRoomList() []byte {
	var buf []byte

	buf = append(buf, 0, 0)
	binary.LittleEndian.PutUint16(buf[:2], uint16(len(Rooms)))

	i:= 0

	for key, value := range Rooms {
		cur:= *value

		name := []byte(cur.Name)

		buf = append(buf, 0, 0, 0, 0)

		binary.LittleEndian.PutUint16(buf[i+2:i+4], uint16(key))
		binary.LittleEndian.PutUint16(buf[i+4:i+6], uint16(len(name)))
		buf = append(buf, name...)

		i = i+4+len(name)
	}
	return buf[:i+2]
}

func UserLobbyIterator() {
	for {
		for key, value := range UserLobby {
			cur := *value

			buf:= make([]byte, 8192)
			bufLen, err := (*cur.Conn).Read(buf)

			if err!= nil {
				continue
			}

			if bufLen > 0 {
				reqType := int(binary.LittleEndian.Uint16(buf[:2]))

				if reqType == 1 {
					room := int(binary.LittleEndian.Uint16(buf[2:4]))
					go AddUserToRoom(*cur.Conn, room, value)
				} else if reqType == 2 {

				}
			}
			fmt.Printf("%d %s\n", key, cur.Name)
		}
		time.Sleep(3)
	}
}

func UserCommander(user *User) {  //when not in room
	cur := *user
	buf:= make([]byte, 8192)

	for {
		bufLen, err := (*cur.Conn).Read(buf)

		if err!= nil {
			continue
		}

		if bufLen > 0 {
			reqType := int(binary.LittleEndian.Uint16(buf[:2]))

			if reqType == 1 {
				room := int(binary.LittleEndian.Uint16(buf[2:4]))
				if AddUserToRoom(*cur.Conn, room, user) {
					break
				}
			} else if reqType == 2 {

			}
		}
	}
}

func UserInRoom(user *User, room *Room, key int ) {
	cur := *user

	buf := make([]byte, 8192)

	ResendStatus(room, key, cur.Name+" connected")

	for {
		reqLen, _ := (*cur.Conn).Read(buf)

		if reqLen == 0 {
			ResendStatus(room, key, cur.Name+" disconnected")
			return
		}

		mesType := binary.LittleEndian.Uint16(buf[:2])

		if mesType == 10 {
			ResendExcluding(room, key, string(buf[2:]))
		}
	}
}

func AddUserToRoom(conn net.Conn, room int, user *User) bool {
	cur := *user

	_, ok := Rooms[room]

	if !ok {
		go SendError(cur.Conn, "Invalid Request")
		return false
	}

	fmt.Printf("User \"%s\" joined room %d: \"%s\"\n", cur.Name, room, Rooms[room].Name)

	Rooms[room].LastUser++
	Rooms[room].Users[Rooms[room].LastUser-1] = user

	UserInRoom(user, Rooms[room], Rooms[room].LastUser-1)

	return true
}

func ResendExcluding(room *Room, excl int, message string) {
	rom := *room

	fmt.Printf("%s", rom.Users[excl].Name+": "+message+"\n")

	for key, value := range rom.Users {
		if key == excl {
			continue
		}
		cur := *value

		SendMessage(cur.Conn, rom.Users[excl].Name+": "+message+"\n")
	}
}

func ResendStatus(room *Room, excl int, message string) {
	rom := *room

	fmt.Printf("%s", message+"\n")

	for key, value := range rom.Users {
		if key == excl {
			continue
		}
		cur := *value

		SendMessage(cur.Conn, message)
	}
}

func SendError(conn *net.Conn, error string) {
	var buf []byte

	buf = append(buf, 0, 0)

	binary.LittleEndian.PutUint16(buf[:2], 65535)

	buf = append(buf, []byte(error)...)

	(*conn).Write(buf)
}

func SendMessage(conn *net.Conn, message string) {
	var buf []byte

	buf = append(buf, 0, 0)

	binary.LittleEndian.PutUint16(buf[:2], 10)

	buf = append(buf, []byte(message)...)

	(*conn).Write(buf)
}

func HandleConnection(conn net.Conn) {
	buf := make([]byte, 8192)
	reqLen, err := conn.Read(buf)

	if err != nil {
		conn.Close()
		return
	}

	resp := buf[:reqLen]

	usLen := binary.LittleEndian.Uint16(resp[:2])

	username := string(resp[2:usLen+2])

	usr := User{Name:username, Conn:&conn}
	conn.Write(GetRoomList())

	UserCommander(&usr)
}
