package main

import "fmt"
import "net"
import "encoding/binary"

func main() {
	conn, err := net.Dial("tcp", "localhost:13646")
	if err != nil {panic(err)}

	defer conn.Close()

	var usName string

	fmt.Printf("Enter Username: ")
	fmt.Scanf("%s\n", &usName)

	name := []byte(usName)

	Register(conn, name)
	PrintRooms(conn)

	var room int

	fmt.Scanf("%d\n", &room)

	EnterRoom(conn, room)

	buf:= make([]byte, 8192)

	go WaitForInput(conn)

	for {
		_, err:= conn.Read(buf)
		if err != nil {
			fmt.Printf("Connection Falied\n")
			return
		}

		mesType := binary.LittleEndian.Uint16(buf[:2])

		if mesType == 10 || mesType == 65535 {
			fmt.Printf("%s\n", string(buf[2:]))
		}
	}
}

func EnterRoom(conn net.Conn, room int) {
	var buf []byte

	buf = append(buf, 0, 0, 0, 0)

	binary.LittleEndian.PutUint16(buf[:2], uint16(1))
	binary.LittleEndian.PutUint16(buf[2:4], uint16(room))

	conn.Write(buf)
}

func WaitForInput(conn net.Conn) {
	var mes string

	for {
		fmt.Scanf("%s\n", &mes)

		SendMessage(&conn, mes)
	}
}

func Register(conn net.Conn, name []byte) {
	var buf []byte
	buf = append(buf, 0, 0)
	binary.LittleEndian.PutUint16(buf[:2], uint16(len(name)))
	buf = append(buf, name...)
	conn.Write(buf)
}

func SendMessage(conn *net.Conn, message string) {
	var buf []byte

	buf = append(buf, 0, 0)

	binary.LittleEndian.PutUint16(buf[:2], 10)

	buf = append(buf, []byte(message)...)

	(*conn).Write(buf)
}


func PrintRooms(conn net.Conn) {
	rbuf := make([]byte, 8192)
	reqLen, err := conn.Read(rbuf)

	if err != nil {
		return
	}

	rbuf = rbuf[:reqLen]
	cnt := binary.LittleEndian.Uint16(rbuf[:2])
	lastByte := 0

	fmt.Printf("Available Rooms: \n")

	for i:= 0; i < int(cnt); i++ {
		key := binary.LittleEndian.Uint16(rbuf[lastByte+2 : lastByte+4])
		strLen := binary.LittleEndian.Uint16(rbuf[lastByte+4 : lastByte+6])

		str := string(rbuf[lastByte+6: uint16(lastByte+6)+strLen])

		lastByte = lastByte + 4 + int(strLen)
		fmt.Printf("%d-%s\n", key, str)
	}
	fmt.Printf("Enter room to join (number): ")
}
